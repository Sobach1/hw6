import os
from flask import Flask, jsonify, request
import time

app_port=8080
app = Flask(__name__)

@app.route("/return_secret_number")
def read_item():
    global secret_num
    time.sleep(.85)
    return jsonify(secret_number=secret_num)

if __name__ == "__main__":
    secret_num = os.getenv("SECRET_NUM", "12345")
    app.run(host="0.0.0.0", port=app_port)
