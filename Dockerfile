FROM python:3.7
RUN pip install flask requests
COPY app.py /
CMD ["python3", "app.py"]
